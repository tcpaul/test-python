SHELL := bash
.SHELLFLAGS := -eu -o pipefail -c  
.ONESHELL:
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

env: env/touchfile

env/touchfile: requirements.txt
> test -d venv || python3 -m venv env
> . env/bin/activate; pip3 install -Ur requirements.txt
> touch env/touchfile

test: env
> . env/bin/activate; pytest .\src\main.py

clean:
> rm -rf env
> find -iname "*.pyc" -delete