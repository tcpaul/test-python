pipeline {
    agent any

    stages {
        stage("Init Build") {
            steps {
                echo "Initializing Build"
                sh "python3 -m venv env"
                sh ". env/bin/activate"
                sh "pip3 install -r requirements.txt"
            }
        }
        stage("Test") {
            steps {
                echo "Running tests"
                sh "make"
            }
        }
        stage("Deploy") {
            steps {
                echo "Deploying projects"
                // sh 'gcloud compute instances create sample-gcloud-app --zone=us-central1-a'
                sh 'gcloud compute ssh --zone us-central1-a sample-gcloud-app --command "git clone https://gitlab.com/tcpaul/test-python.git"'
                sh 'gcloud compute ssh --zone us-central1-a sample-gcloud-app --command "cd test-python && python3 -m venv env && . env/bin/activate && pip3 install -Ur requirements.txt"'
            }
        }
        stage ('Run') {
            steps {
                echo "Run python code"
                sh 'gcloud compute ssh --zone us-central1-a sample-gcloud-app --command "cd test-python && python3 src/main.py"'
                mail from: "rollin.metz16@ethereal.email",
                    cc: "rollin.metz16@ethereal.email",
                    bcc: "rollin.metz16@ethereal.email",
                    to: "rollin.metz16@ethereal.email",
                    replyTo: "rollin.metz16@ethereal.email",
                    subject: "Jenkins Mail: [Code Deployed] Job '${JOB_NAME}' (${BUILD_NUMBER})",
                    body: """
                    Mail sent from jenkins. Code has been successfully deployed. Please go to ${BUILD_URL} and verify the build
                    BUILD_URL: ${BUILD_URL}
                    JOB_URL: ${JOB_URL}
                    WORKSPACE: ${WORKSPACE}
                    WORKSPACE_TMP: ${WORKSPACE_TMP}
                    JENKINS_HOME: ${JENKINS_HOME}
                    JENKINS_URL: ${JENKINS_URL}
                    // CI: ${CI}
                    BUILD_NUMBER: ${BUILD_NUMBER}
                    BUILD_ID: ${BUILD_ID}
                    """
            }
        }
    }

    post {
        always {
            echo 'CLEANUP: Start'
            deleteDir() /* clean up our workspace */
            sh 'gcloud compute ssh --zone us-central1-a sample-gcloud-app --command "rm -rf test-python"'
        }
        success {
            echo 'BUILD: SUCCESS'
            mail from: "rollin.metz16@ethereal.email",
                cc: "rollin.metz16@ethereal.email",
                bcc: "rollin.metz16@ethereal.email",
                to: "rollin.metz16@ethereal.email",
                replyTo: "rollin.metz16@ethereal.email",
                subject: "Jenkins Mail: Job '${JOB_NAME}' (${BUILD_NUMBER}) [BUILD SUCCESS]",
                body: """
                BUILD SUCCESS. Please go to ${BUILD_URL} and verify the build
                """
        }
        unstable {
            echo 'BUILD: UNSTABLE'
            mail from: "rollin.metz16@ethereal.email",
                cc: "rollin.metz16@ethereal.email",
                bcc: "rollin.metz16@ethereal.email",
                to: "rollin.metz16@ethereal.email",
                replyTo: "rollin.metz16@ethereal.email",
                subject: "Jenkins Mail: Job '${JOB_NAME}' (${BUILD_NUMBER}) [BUILD UNSTABLE]",
                body: """
                BUILD UNSTABLE. Please go to ${BUILD_URL} and verify the build
                """
        }
        failure {
            echo 'BUILD: FAILED'
            mail from: "rollin.metz16@ethereal.email",
                cc: "rollin.metz16@ethereal.email",
                bcc: "rollin.metz16@ethereal.email",
                to: "rollin.metz16@ethereal.email",
                replyTo: "rollin.metz16@ethereal.email",
                subject: "Jenkins Mail: Job '${JOB_NAME}' (${BUILD_NUMBER}) [BUILD FAILED]",
                body: """
                BUILD FAILED. Please go to ${BUILD_URL} and verify the build
                """
        }
        changed {
            echo 'BUILD: CHANGED'
            mail from: "rollin.metz16@ethereal.email",
                cc: "rollin.metz16@ethereal.email",
                bcc: "rollin.metz16@ethereal.email",
                to: "rollin.metz16@ethereal.email",
                replyTo: "rollin.metz16@ethereal.email",
                subject: "Jenkins Mail: Job '${JOB_NAME}' (${BUILD_NUMBER}) [BUILD CHANGED]",
                body: """
                BUILD CHANGED. Please go to ${BUILD_URL} and verify the build
                """
        }
    }
}
