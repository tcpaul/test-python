def sum(a, b):
    return a + b

def test_sum_correct():
    assert sum(3, 2) == 5

def test_sum_incorrect():
    assert sum(3, 3) != 5

if __name__ == '__main__':
    print('Sum: ',sum(10,20))